import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={'Conectando'}>
      <App />
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root')
);