import React, { useState, useEffect } from 'react'
import utils from '../../utils/utls'

const QuizResult = ({ e, i, questions }) => {

    const initialState = {
        score: 0,
        color: '',
        questions: questions
    };

    const [state, setState] = useState(initialState);

    const init = () => {
        console.log(state)
        const color = utils.getRandomColor();
        let count = 0;
        questions.map( (q, i) => {
            if( q.correct === e.answers[i].answer ){
                count++
            }
        })
        const score = count * (10 / e.answers.length)
        setState({
            ...state,
            color,
            score
        })
    }

    useEffect(() => {
        init();
    }, [])

    return (
        <div key={i} className="col-md-4 my-2">
            <div className={`card bg-${state.color}`}>
                <div className="card-header text-white">
                    {e.name} - {state.score}
                </div>
                <ul className="list-group list-group-flush">
                    {
                        e.answers.map((a, i) => (
                            state.questions[i].correct === a.answer 
                            ? <li key={i} className="list-group-item">{a.question} - {a.answer} <i className="fas fa-check text-success"></i></li>
                            : <li key={i} className="list-group-item">{a.question} - {a.answer} <i className="fas fa-times text-danger"></i></li>
                        ))
                    }
                </ul>
            </div>
        </div>
    )
}

export default QuizResult
