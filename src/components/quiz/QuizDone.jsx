import React, { useContext, useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import UserContext from '../../context/User/UserContext'
import { getDatabase, ref, onValue, get, child } from "firebase/database"
import Navbar from '../dashboard/Navbar';
import QuizResult from './QuizResult';

const QuizDone = () => {
    const { user } = useContext(UserContext);

    const { quiz } = useParams();

    const initialState = {
        done: [],
        questions: []
    }

    const [state, setState] = useState(initialState)

    const db = getDatabase();

    const init = () => {
        const starCountRef = ref(db, `/${user.userData.uid}/${quiz}/done`);
        onValue(starCountRef, (snapshot) => {
            const data = []
            snapshot.forEach(e => {
                data.push(e.val())
            })

            const dbRef = ref(getDatabase());
            get(child(dbRef, `/${user.userData.uid}/${quiz}/questions`)).then((snapshot) => {
                if (snapshot.exists()) {
                    const qs = []
                    snapshot.forEach(e => {
                        qs.push(e.val())
                    })
                    setState({
                        ...state,
                        done: data,
                        questions: qs
                    })
                } else {
                    console.log("No data available");
                }
            }).catch((error) => {
                console.error(error);
            });

        },);
    }

    useEffect(() => {
        init();
    }, [])


    return (
        <>
            {
                !user
                    ? (
                        <h1> No User </h1>
                    )
                    : (
                        <>
                            <Navbar
                                userName={user.userData.displayName}
                                photo={user.userData.photoURL}
                            />
                            <div className="container my-5">
                                <div className="row">
                                    {
                                        state.done.map((e, i) => (
                                            <QuizResult
                                                e={e}
                                                i={i}
                                                questions={state.questions}
                                            />
                                        ))
                                    }
                                </div>
                            </div>
                        </>
                    )
            }
        </>
    )
}

export default QuizDone
