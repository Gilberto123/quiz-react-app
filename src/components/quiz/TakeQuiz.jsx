import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { getDatabase, ref, child, get, set } from "firebase/database";
import Swal from 'sweetalert2'
import Navbar from '../Navbar';

const TakeQuiz = () => {

    const initialState = {
        date: '',
        state: 'loading',
        quiz: {},
        answers: []
    }

    const [state, setState] = useState(initialState)

    const { room, quiz, name } = useParams();

    const history = useHistory();

    const handleSelected = (e) => {

        const question = e.target.name

        const answer = e.target.value

        const obj = {
            question,
            answer
        }

        let index = -1;
        state.answers.map((e, i) => {
            if (e.question === question) {
                index = i
            }
        })

        const ans = state.answers;

        if (index === -1) {
            ans.push(obj)
        } else {
            ans[index] = obj
        }

        setState({
            ...state,
            answers: ans
        });
    }

    const saveQuizToDataBase = () => {

        if (state.answers.length !== state.quiz.questions.length) {
            Swal.fire({
                title: 'Error!',
                text: 'Missing answers...',
                icon: 'error',
            });
            return
        }

        Swal.fire({
            title: 'Saving answers...',
            text: 'Please Waite!',
            icon: 'info',
            showConfirmButton: false
        });

        const obj = {
            name: name.toUpperCase(),
            answers: state.answers
        }

        const db = getDatabase();
        set(ref(db, `/${room}/${quiz}/done/${state.date}`), obj).then(() => {
            Swal.close();
            Swal.fire({
                title: 'Quiz Saved',
                text: '',
                icon: 'success'
            });
            setState({
                ...state,
                state: 'loading'
            });
            history.push('/');
        })

    }

    const init = () => {

        const dbRef = ref(getDatabase());

        get(child(dbRef, `/${room}/${quiz}`)).then((snapshot) => {

            if (snapshot.exists()) {

                const date = new Date();
                const now = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`

                const qz = snapshot.val();

                setState({
                    ...state,
                    quiz: qz,
                    state: 'ready',
                    date: now
                })

            } else {

                console.log("No data available");

                setState({
                    ...state,
                    quiz: [],
                    state: 'ready'
                })

            }

        }).catch((error) => {

            console.error(error);

        });

    }

    useEffect(() => {
        init();
    }, [])

    return (
        <>
            {
                state.state === 'loading'
                    ? (
                        <h2>Please Wait</h2>
                    )
                    : (
                        <>
                            <Navbar />
                            <div className="container">
                                <div className="row justify-content-center my-5">
                                    <div className="col-md-10">

                                        <div className="card">

                                            <div className="card-body">
                                                <h2>
                                                    {state.quiz.name} - {name}
                                                </h2>
                                                <ul>
                                                    {
                                                        state.quiz.questions.map((qs, i) => (
                                                            <>
                                                                <p>{qs.q}</p>
                                                                <div className="form-check">
                                                                    <input onClick={handleSelected} className="form-check-input" value='a' name={qs.q} type="radio" id="exampleRadios1" />
                                                                    <label className="form-check-label" htmlFor="exampleRadios1">
                                                                        {qs.a}
                                                                    </label>
                                                                </div>
                                                                <div className="form-check">
                                                                    <input onClick={handleSelected} className="form-check-input" value='b' name={qs.q} type="radio" id="exampleRadios1" />
                                                                    <label className="form-check-label" htmlFor="exampleRadios1">
                                                                        {qs.b}
                                                                    </label>
                                                                </div>
                                                                <div className="form-check">
                                                                    <input onClick={handleSelected} className="form-check-input" value='c' name={qs.q} type="radio" id="exampleRadios1" />
                                                                    <label className="form-check-label" htmlFor="exampleRadios1">
                                                                        {qs.c}
                                                                    </label>
                                                                </div>
                                                            </>
                                                        ))
                                                    }
                                                </ul>
                                            </div>
                                            <button onClick={saveQuizToDataBase} className='btn btn-primary'> Send </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </>
                    )

            }

        </>
    )

}

export default TakeQuiz
