import React, { useState, useEffect, useContext } from 'react'
import Navbar from './Navbar.jsx'
import config from '../firebase-config'
import { initializeApp } from 'firebase/app'
import { getAuth, signInWithRedirect, getRedirectResult, GoogleAuthProvider } from "firebase/auth"
import { Link, useHistory } from 'react-router-dom'
import loading from '../loading.gif'

import UserContext from '../context/User/UserContext'

const Auth = () => {

    const { logIn, user } = useContext(UserContext);

    const initialState = {
        status: 'loading'
    }

    const [state, setState] = useState(initialState);

    const history = useHistory();

    const firebaseConfig = config;

    initializeApp(firebaseConfig);

    const provider = new GoogleAuthProvider();

    const auth = getAuth();

    const signIn = () => {
        signInWithRedirect(auth, provider);
    }

    const init = () => {

        if (user === 'loggedOut') {
            setState({
                ...state,
                status: 'ready'
            });
            return;
        }
        getRedirectResult(auth)
            .then((result) => {
                setState({
                    ...state,
                    status: 'loading'
                });
                // This gives you a Google Access Token. You can use it to access Google APIs.
                const credential = GoogleAuthProvider.credentialFromResult(result);
                /* const token = credential.accessToken; */

                // The signed-in user info.
                const user = result.user;

                const userState = {
                    userData: user,
                    credential
                }
                logIn(userState);
                history.push(`/dashboard`);
            }).catch((error) => {
                /*  // Handle Errors here.
                 const errorCode = error.code;
                 const errorMessage = error.message;
                 // The email of the user's account used.
                 const email = error.email;
                 // The AuthCredential type that was used.
                 const credential = GoogleAuthProvider.credentialFromError(error);
                 // ... */
                console.log(error);
                setState({
                    ...state,
                    status: 'ready'
                });
            });

    }

    useEffect(() => {
        //TODO: Allow Domain to use from mobil
        init();
    }, [])

    return (
        <>
            {
                state.status === 'ready'
                    ? (
                        <>
                            <Navbar />
                            <div className='container auth-container d-flex flex-column justify-content-center align-items-center'>

                                <button className='btn btn-medium btn-primary' onClick={signIn}>
                                    Sign In With Google
                                    <i className="fab fa-google mx-3"></i>
                                </button>
                                <Link className='btn btn-secondary mt-5' to='/quiz'>
                                    Answer a Quiz
                                    <i className="fas fa-clipboard-list mx-3"></i>
                                </Link>
                            </div>
                        </>
                    )
                    : (
                        <div className="container justify-content-center">
                            <div className="row">
                                <div className="col-md-12 d-flex justify-content-center">
                                    <img className='img-fluid' src={loading} alt="loading" />
                                </div>
                            </div>
                        </div>
                    )
            }

        </>
    )
}

export default Auth
