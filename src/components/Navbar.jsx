import React from 'react'

const Navbar = () => {
    return (
        <nav className="navbar text-white bg-primary justify-content-center">
            <span className="navbar-brand mb-0 h1">React Quiz App</span>
        </nav>
    )
}

export default Navbar
