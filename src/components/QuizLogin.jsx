import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { getDatabase, ref, get } from "firebase/database";
import Swal from 'sweetalert2'
import Navbar from './Navbar'

const QuizLogin = () => {

    const history = useHistory();

    const initialState = {
        name: '',
        room: '',
        quiz: ''
    }

    const [state, setState] = useState(initialState)

    const handleInputChange = e => {
        setState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = e => {

        e.preventDefault();

        const db = getDatabase();

        get((ref(db, `${state.room}/${state.quiz}/done`)))
            .then(s => {

                if (s.exists()) {
                    let verify = false;
                    s.forEach(e => {
                        if (e.val().name === state.name.toUpperCase()) {
                            verify = true;
                            return;
                        }
                    });

                    if (verify) {
                        Swal.fire({
                            title: 'Login Error!',
                            text: 'Your Quiz was done',
                            icon: 'error',
                        });
                        history.push('/');
                    } else {
                        const route = `/quiz/${state.room}/${state.quiz}/${state.name}`
                        history.push(route);
                    }

                } else {
                    const route = `/quiz/${state.room}/${state.quiz}/${state.name}`
                    history.push(route);
                }

            })

    }
    return (
        <>
            <Navbar />
            <form onSubmit={handleSubmit} id='quizform' className='container'>

                <div id='quizformrow' className="row justify-content-center align-items-center">

                    <div className="col-md-4">

                        <div className="form-group">
                            <label htmlFor="name">Your Name</label>
                            <input onChange={handleInputChange} name='name' value={state.name} type="text" className="form-control" id="name" required />
                        </div>

                        <div className="form-group">
                            <label htmlFor="room">Room ID</label>
                            <input onChange={handleInputChange} name='room' value={state.room} type="text" className="form-control" id="room" required />
                        </div>

                        <div className="form-group">
                            <label htmlFor="quiz">Quiz ID</label>
                            <input onChange={handleInputChange} name='quiz' value={state.quiz} type="text" className="form-control" id="quiz" required />
                        </div>

                        <button type="submit" className="btn btn-block btn-primary">Let's Go</button>
                    </div>

                </div>

            </form>
        </>
    )
}

export default QuizLogin
