import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../context/User/UserContext';
import { Link } from 'react-router-dom';
import utils from '../../utils/utls';

const QuizCard = ({ e, deleteQuiz }) => {

    const initialState = {
        color: ''
    }

    const [state, setState] = useState(initialState);

    const { user } = useContext(UserContext);

    const getColor = () => {
        const color = utils.getRandomColor();
        console.log(color)
        setState({
            color
        });
    }

    useEffect(() => {
        getColor();
    }, [])

    return (
        <div key={e.key} className="col-md-3 my-4">
            <div className={`card bg-${state.color}`}>
                <div className="card-body">
                    <h5 className="card-title text-white">{e.data.name}</h5>
                    <hr />
                    <p className="card-text text-white">ROOM: {user.userData.uid}</p>
                    <p className="card-text text-white">ID: {e.key}</p>
                </div>
                <ul className={`list-group list-group-flush bg-${state.color}`}>
                    {
                        e.data.questions.map((a, i) => (
                            <li key={i} className="list-group-item">{a.q} - {a.correct}</li>
                        ))
                    }
                </ul>
                <div className="card-body">
                    <div className="row flex-column">
                        <div className="col-md-12">
                            <Link to={`/quiz/done/${e.key}`} className="btn btn-block btn-primary mt-1">
                                View Done
                            </Link>
                        </div>
                        <div className="col-md-12">
                            <button onClick={() => deleteQuiz(e.key)} className="btn btn-block btn-danger mt-1">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default QuizCard
