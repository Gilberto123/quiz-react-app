import React, { useState, useEffect } from 'react'
import utils from '../../utils/utls'

const QuestionCard = ({qs, i, setEditQuestionMode, handleDeleteQuestion}) => {

    const initialState = {
        color: ''
    }

    const [state, setState] = useState(initialState);

    const getColor = () => {
        const color = utils.getRandomColor();
        setState({
            color
        });
    }

    useEffect(() => {
        getColor();
    }, [])
    return (
        <div key={i} className="col-md-12 mt-2">
            <div className={`card bg-${state.color}`}>
                <div className="card-header">
                    <div className="row">
                        <div className="col-md-12 d-flex justify-content-between text-white">
                            {qs.q}
                            <div className="card-tools">
                                <button onClick={() => setEditQuestionMode(i)} className='btn btn-success mx-1 my-1'>
                                    <i className="fas fa-edit"></i>
                                </button>
                                <button onClick={() => handleDeleteQuestion(i)} className="btn btn-danger mx-1 my-1">
                                    <i className="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">{qs[qs.correct]}</li>
                </ul>
            </div>
        </div>
    )
}

export default QuestionCard
