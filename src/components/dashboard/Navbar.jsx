import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import UserContext from '../../context/User/UserContext'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify';

import { getAuth, signOut } from "firebase/auth";

const Navbar = (props) => {
    const auth = getAuth();
    const { logOut } = useContext(UserContext);
    const history = useHistory();
    const handleLogout = () => {
        logOut();
        history.push('/');
        signOut(auth).then(() => {
            // Sign-out successful.
        }).catch((error) => {
            // An error happened.
            console.log(error)
        });
        toast.success('Loggedout');
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <Link to='#' className="navbar-brand">
                React Quiz App
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to='#' className="nav-link active mr-5">
                            {props.userName}
                            <img src={props.photo} alt="avatar" className="rounded-circle avatar" />
                        </Link>
                    </li>
                    <li className="nav-item d-flex align-items-center">
                        <button onClick={handleLogout} className="btn btn-danger">
                            Logout
                            <i className="fas fa-sign-out-alt ml-2"></i>
                        </button>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Navbar
