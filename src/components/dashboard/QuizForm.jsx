import React, { useState, useContext, useEffect } from 'react'
import { getDatabase, ref, set, onValue, remove } from "firebase/database"
import UserContext from '../../context/User/UserContext'
import Swal from 'sweetalert2'
import { toast } from 'react-toastify'
import QuestionCard from './QuestionCard'
import QuizCard from './QuizCard'

const QuizForm = () => {

    const initialState = {
        q: '',
        a: '',
        b: '',
        c: '',
        correct: 'a',
        qzname: '',
        quiz: [],
        questionStatus: 'add',
        editingIndex: -1,
        quizes: [],
        done: []
    }

    const [state, setState] = useState(initialState);

    const { user } = useContext(UserContext);

    const database = getDatabase();

    const init = () => {

        const starCountRef = ref(database, `/${user.userData.uid}`);

        onValue(starCountRef, (snapshot) => {

            let arr = [];

            snapshot.forEach(e => {
                const obj = {
                    data: e.val(),
                    key: e.key
                }
                arr.push(obj);
            });

            console.log(arr)

            setState({
                ...state,
                q: '',
                a: '',
                b: '',
                c: '',
                quiz: [],
                questionStatus: 'add',
                editingIndex: -1,
                quizes: arr.reverse()
            });

        });

    }

    useEffect(() => {
        init();
    }, [database, user.userData.uid])

    const handleInputChange = (e) => {
        setState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    const handleSelectAnswer = (e) => {
        setState({
            ...state,
            correct: `${e.target.value}`
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (state.quiz.length >= 4) {
            Swal.fire({
                title: 'Sorry!',
                text: '4 Questions Maximun',
                icon: 'info',
                confirmButtonText: 'Ok!',
            });
            return
        }

        toast.success('Question added');


        const newQuestion = {
            q: state.q,
            a: state.a,
            b: state.b,
            c: state.c,
            correct: state.correct
        }

        const newQuiz = state.quiz;
        newQuiz.push(newQuestion);

        setState({
            ...state,
            q: '',
            a: '',
            b: '',
            c: '',
            quiz: newQuiz
        });

    }

    const handleDeleteQuestion = async (index) => {

        const confirm = await Swal.fire({
            title: 'Confirm!',
            text: 'Do you want to continue?',
            icon: 'question',
            confirmButtonText: 'Yes!',
            showDenyButton: 'true',
            denyButtonText: 'Cancel!'
        });

        if (confirm.isConfirmed) {
            const tempQuiz = state.quiz;
            const newQuiz = tempQuiz.filter((e, i) => i !== index);
            setState({
                ...state,
                quiz: newQuiz
            });
            Swal.fire({
                title: 'Deleted!',
                icon: 'success',
                confirmButtonText: 'Yes!',
            });
            return;
        }

        Swal.fire({
            title: 'Ok!',
            text: 'Question no deleted',
            icon: 'info'
        });

    }

    const setEditQuestionMode = (index) => {
        const tempQ = state.quiz[index];
        const opt = document.getElementById(`gridRadios${tempQ.correct}`);
        opt.checked = true;
        setState({
            ...state,
            q: tempQ.q,
            a: tempQ.a,
            b: tempQ.b,
            c: tempQ.c,
            correct: tempQ.correct,
            questionStatus: 'edit',
            editingIndex: index
        })
    }

    const handleSaveQuestionChanges = () => {
        const index = state.editingIndex;
        const tempQ = state.quiz;
        const tempQs = {
            q: state.q,
            a: state.a,
            b: state.b,
            c: state.c,
            correct: state.correct
        }
        tempQ[index] = tempQs;
        setState({
            ...state,
            q: '',
            a: '',
            b: '',
            c: '',
            quiz: tempQ,
            questionStatus: 'add',
            editingIndex: -1
        });
        toast.success('Saved');
    }

    const handleSaveQuizToDataBase = () => {

        if (state.quizes.length >= 4) {

            Swal.fire({
                title: 'Sorry!',
                text: 'You can´t have more quizes...',
                icon: 'error',
            });

            document.querySelector('#quizform').reset();

            setState({
                ...state,
                q: '',
                a: '',
                b: '',
                c: '',
                correct: 'a',
                quiz: [],
                qzname: '',
                questionStatus: 'add',
                editingIndex: -1,
            })

            return
        }

        Swal.fire({
            title: 'Saving Quiz...',
            text: 'Please Wait...',
            icon: 'info',
            showConfirmButton: false,
        });

        const date = Date.now();

        const newQuiz = {
            questions: state.quiz,
            name: state.qzname
        }

        set(ref(database, `${user.userData.uid}/${date}`), newQuiz)
            .then(() => {
                toast.success('Quiz Saved');
                document.querySelector('#quizform').reset();
                Swal.close();
            });

        document.querySelector('#myquizes').scrollIntoView();
    }

    const deleteQuiz = async (id) => {

        const confirm = await Swal.fire({
            title: 'Confirm!',
            text: 'Do you want to continue?',
            icon: 'question',
            confirmButtonText: 'Yes!',
            showDenyButton: 'true',
            denyButtonText: 'Cancel!'
        });

        if (confirm.isConfirmed) {
            const deleteRef = ref(database, `/${user.userData.uid}/${id}`);
            remove(deleteRef).then(data => {
                Swal.fire({
                    title: 'Deleted!',
                    icon: 'success',
                    confirmButtonText: 'Yes!',
                });
            });
            return;
        }

        Swal.fire({
            title: 'Ok!',
            text: 'Quiz no deleted',
            icon: 'info'
        });
    }


    return (
        <div className='container'>
            <div className="row mt-5 justify-content-around">
                <div className="col-md-6">
                    <h2>
                        New Quiz Form
                    </h2>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <label htmlFor="quizname">Quiz Name</label>
                            <input onChange={handleInputChange} name='qzname' value={state.qzname} type="text" className="form-control" id="quizname" required autoFocus />
                        </div>
                    </div>
                    <form id='quizform' onSubmit={handleSubmit}>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <label htmlFor="question">Question</label>
                                <input onChange={handleInputChange} name='q' value={state.q} type="text" className="form-control" id="question" required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="opta">Option A</label>
                            <input onChange={handleInputChange} name='a' value={state.a} type="text" className="form-control" id="opta" required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="optB">Option B</label>
                            <input onChange={handleInputChange} name='b' value={state.b} type="text" className="form-control" id="optb" required />
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <label htmlFor="optC">Option C</label>
                                <input onChange={handleInputChange} name='c' value={state.c} type="text" className="form-control" id="optc" required />
                            </div>
                        </div>
                        <fieldset className="form-group">
                            <div className="row">
                                <legend className="col-form-label col-sm-2 pt-0">Correct Answer</legend>
                                <div className="col-sm-10 d-flex flex-row justify-content-around">
                                    <div className="form-check">
                                        <input onClick={handleSelectAnswer} value='a' className="form-check-input" type="radio" name="gridRadios" id="gridRadiosa" defaultChecked />
                                        <label className="form-check-label" htmlFor="gridRadiosa">
                                            A
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input onClick={handleSelectAnswer} value='b' className="form-check-input" type="radio" name="gridRadios" id="gridRadiosb" />
                                        <label className="form-check-label" htmlFor="gridRadiosb">
                                            B
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input onClick={handleSelectAnswer} value='c' className="form-check-input" type="radio" name="gridRadios" id="gridRadiosc" />
                                        <label className="form-check-label" htmlFor="gridRadiosc">
                                            C
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        {
                            state.questionStatus === 'add'
                                ? (
                                    <div className='row justify-content-center mb-5'>
                                        <button type="submit" className="btn btn-primary">Push Quiestion</button>
                                        {
                                            state.quiz.length > 1 && <button onClick={handleSaveQuizToDataBase} type="button" className="ml-3 btn btn-secondary">Save Quiz</button>
                                        }
                                    </div>
                                )
                                : (
                                    <button onClick={handleSaveQuestionChanges} type="button" className="btn btn-success">Save Changes</button>
                                )
                        }
                    </form>

                </div>
                <div className="col-md-6">
                    <div className="row justify-content-around">
                        {
                            state.quiz.length <= 0
                            && (
                                <h1>No Questions Yet</h1>
                            )
                        }
                        {
                            state.quiz.map((qs, i) => (
                                <QuestionCard
                                    key={i}
                                    qs={qs}
                                    i={i}
                                    setEditQuestionMode={setEditQuestionMode}
                                    handleDeleteQuestion={handleDeleteQuestion}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
            <hr />
            <div className="row mt-5 d-flex">
                <div id='myquizes' className="col-md-12 mt-5">
                    <h2>My Quizes</h2>
                </div>
                {
                    state.quizes.length === 0 && <h2 className='my-5 mx-auto'>No Quizes Yet</h2>
                }
                {
                    state.quizes.map((e) => (
                        <QuizCard
                            key={e.key}
                            e={e}
                            deleteQuiz={deleteQuiz}
                        />
                    ))
                }
            </div>
        </div>
    )
}

export default QuizForm
