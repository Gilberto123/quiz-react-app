import React, { useContext } from 'react'
import UserContext from '../context/User/UserContext'
import Navbar from './dashboard/Navbar';
import QuizForm from './dashboard/QuizForm';

const Dashboard = () => {

    const { user } = useContext(UserContext);

    return (

        <>
            {
                !user
                    ? (
                        <h1> No User </h1>
                    )
                    : (
                        <>
                            <Navbar
                                userName={user.userData.displayName}
                                photo={user.userData.photoURL}
                            />
                            <QuizForm />
                        </>
                    )
            }
        </>

    )
}

export default Dashboard
