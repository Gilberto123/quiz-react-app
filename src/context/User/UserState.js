/*  */
import React, { useReducer } from 'react'
import UserReducer from './UserReducer'
import UserContext from './UserContext'

const UserState = (props) => {
    const initialState = {
        user: null
    }

    const [state, dispatch] = useReducer(UserReducer, initialState)

    const logIn = (user) => {
        dispatch({
            type: 'LOG_IN',
            payload: user
        });
    }

    const logOut = () => {
        dispatch({
            type: 'LOG_OUT',
            payload: 'loggedOut'
        });
    }

    return (
        <UserContext.Provider value={{
            user: state.user,
            logIn,
            logOut
        }}>
            {props.children}
        </UserContext.Provider>
    )
}

export default UserState;