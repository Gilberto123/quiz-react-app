import { LOG_IN, LOG_OUT } from '../types'

const reducer = (state, action) => {
    const { payload, type } = action;

    switch (type) {
        case LOG_IN:
            return {
                ...state,
                user: payload
            }
        case LOG_OUT:
            return {
                ...state,
                user: payload
            }
        default:
            return state
    }
}

export default reducer