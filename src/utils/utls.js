const colors = ['primary','secondary','success', 'info']

const getRandomColor = () => {
    const random = Math.floor((Math.random() * 4))
    return colors[random]
}

const utils = {
    getRandomColor
}

export default utils;