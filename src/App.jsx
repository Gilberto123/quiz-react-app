import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Auth from "./components/Auth";
import Dashboard from './components/Dashboard';
import './styles.css'
import UserState from './context/User/UserState'
import QuizLogin from './components/QuizLogin';
import TakeQuiz from './components/quiz/TakeQuiz';
import QuizDone from './components/quiz/QuizDone';

function App() {

  return (
    <UserState>
      <Router>

        <Switch>

          <Route path='/quiz/:room/:quiz/:name'>
            <TakeQuiz />
          </Route>

          <Route path='/quiz/done/:quiz'>
            <QuizDone />
          </Route>

          <Route path='/quiz'>
            <QuizLogin />
          </Route>

          <Route path='/dashboard'>
            <Dashboard />
          </Route>

          <Route path='/'>
            <Auth />
          </Route>

        </Switch>

      </Router>

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

    </UserState>

  );
}

export default App;
